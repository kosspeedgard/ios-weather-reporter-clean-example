//
//  GraphWeatherCoordinate.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct GraphWeatherCoordinate: GraphDecoder {
    var longitude: Double?
    var latitude: Double?
    
    init(json: JSON?) {
        self.longitude = json?["lon"] as? Double
        self.longitude = json?["lat"] as? Double
    }
    
    func toJson() -> JSON? {
        return nil
    }
}
