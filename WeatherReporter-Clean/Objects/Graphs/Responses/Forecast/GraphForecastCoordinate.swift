//
//  GraphForecastCoordinate.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct GraphForecastCoordinate: GraphDecoder {
    var latitude: Double?
    var longitude: Double?
    
    init(json: JSON?) {
        self.latitude = json?["lat"] as? Double
        self.longitude = json?["lon"] as? Double
    }
    
    func toJson() -> JSON? {
        return nil
    }
}
