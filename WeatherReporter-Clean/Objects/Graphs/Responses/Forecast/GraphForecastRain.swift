//
//  GraphForecastRain.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct GraphForecastRain: GraphDecoder {
    var _3h: Double?
    
    init(json: JSON?) {
        self._3h = json?["3h"] as? Double
    }
    
    func toJson() -> JSON? {
        return nil
    }
}
