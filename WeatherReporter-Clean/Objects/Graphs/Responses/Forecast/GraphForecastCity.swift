//
//  GraphForecastCity.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct GraphForecastCity: GraphDecoder {
    var id: Int?
    var name: String?
    var coordinate: GraphForecastCoordinate?
    var country: String?
    var population: Int?
    
    init(json: JSON?) {
        self.id = json?["id"] as? Int
        self.name = json?["name"] as? String
        self.coordinate = GraphForecastCoordinate(json: json?["coord"] as? JSON)
        self.country = json?["country"] as? String
        self.population = json?["population"] as? Int
    }
    
    func toJson() -> JSON? {
        return nil
    }
}
