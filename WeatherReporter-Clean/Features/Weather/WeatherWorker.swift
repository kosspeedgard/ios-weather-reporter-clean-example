//
//  WeatherWorker.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

//MARK: FIX
//Inherite BaseWorker for use ability in base class
class WeatherWorker {
    func fetchWeather(city: String?, completion: @escaping(_ weatherBase: GraphWeatherBase?) -> Void, failure: @escaping(_ error: GraphError?) -> Void) {
        //MARK: FIX
        /*
            Implement send request api by use GET method
         1.UrlSuffix: URLSuffixs.Weather.weather + "q=\(city ?? "bangkok")"
         2.ResponseType: GraphWeatherBase
         
            When success pass data by completion
            When fail pass error by failure
         */
    }
    
    func fetchForecast(city: String?, completion: @escaping(_ forecastBase: GraphForecastBase?) -> Void, failure: @escaping(_ error: GraphError?) -> Void) {
        super.sendRequestWith(method: .GET, host: .baseURL, urlSuffixs: URLSuffixs.Weather.forecast + "q=\(city ?? "bangkok")&cnt=7", json: nil, responseType: GraphForecastBase.self, completion: { (forecastBase) in
            completion(forecastBase)
        }, failure: { (error) in
            failure(error)
        })
    }
}
