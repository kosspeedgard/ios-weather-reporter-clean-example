//
//  WeatherProtocols.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

//MARK: ViewController
protocol WeatherDisplayable: class, AppDisplayable {
    func displayWeatherInformation(viewModel: Weather.WeatherSection.ViewModel)
    func displayForecastInformation(viewModel: Weather.ForecastSection.ViewModel)
    func displayedAlert()
}

//MARK: Interactor
protocol WeatherBusinessLogic {
    func fetchWeatherInformation(request: Weather.WeatherSection.Request)
    func fetchForecastInformation(request: Weather.ForecastSection.Request)
}

//MARK: Presenter
protocol WeatherPresentable {
    func presentWeatherInformationSuccess(response: Weather.WeatherSection.Response)
    func presentForecastInformationSuccess(response: Weather.ForecastSection.Response)
    func presentInformationFailure()
}

//MARK: Router
protocol WeatherRoutable {
    
}
