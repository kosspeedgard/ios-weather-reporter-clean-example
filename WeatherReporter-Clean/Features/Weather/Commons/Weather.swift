//
//  Weather.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct Weather {
    struct WeatherSection {
        struct Request {
            let city: String
            let isCelsius: Bool
        }
        
        struct Response {
            let weather: GraphWeatherBase
            let isCelsius: Bool
        }
        
        struct ViewModel {
            let weather: WeatherViewModel
        }
        
        struct WeatherViewModel {
            let temperature: String
            let humidity: String
            let wind: String
            let cityName: String
            let temperatureType: String
        }
    }
    
    struct ForecastSection {
        struct Request {
            let city: String
            let isCelsius: Bool
        }
        
        struct Response {
            let forecasts: [GraphForecastData]
            let isCelsius: Bool
        }
        
        struct ViewModel {
            let forecasts: [ForecastViewModel]
        }
    
        struct ForecastViewModel {
            let temperature: String
            let humidity: String
            let wind: String
            let day: String
            let description: String
        }
    }
}
