//
//  Utils.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

class Utils: NSObject {
    static func timeFromUtils(time: Int?, format: String) -> String? {
        return Date(unix: time ?? 0).string(format: format)
    }
    
    //MARK: FIX
    static func covertToCelsius(kelvin: Double) -> Int {
        //Implement kelvin -> celsius converter
        return 0
    }
    
    static func convertToFarenheit(kelvin: Double) -> Int {
        //Implement kelvin -> farenheit converter
        return 0
    }
}
